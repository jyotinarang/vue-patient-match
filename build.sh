#!/bin/bash -e

npm ci
npm run test:unit
npm run lint
npm run build