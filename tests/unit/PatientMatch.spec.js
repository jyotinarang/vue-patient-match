import { mount } from "@vue/test-utils";
import PatientMatch from "@/components/PatientMatch.vue";

describe("PatientMatch.vue", () => {
  let wrapper = null;

  beforeEach(() => {
    wrapper = mount(PatientMatch);
  });

  it("Patient Match App loading text is showing", () => {
    expect(wrapper.text()).toMatch("Loading..");
  });
});
