# vue-patient-match
This project builds the patient-match WebComponent element that is used in forView to find matching patients in
different communities. These patients can then be linked/unlinked with existing patient.


## Project setup
Install all dependencies first before starting development.
```
npm install
```
The `npm run build` command has been updated to build the WebComponent which is placed in the /dist folder. 
The details of the WebComponent build is shown. To run the build use the command below. 
This will run lint, tests and build the WebComponent
```
./build.sh
```

Jenkins does an additional step where it bumps the version and publishes the WebComponent in the NPM registry:
```
npm version patch
npm publish
```

## Building the WebComponent
This section is not relevant if you want to make changes to the code. 
But relevant in case you need to update or understand the build flow.
The following command will build the patient-match WebComponent
```
npx vue-cli-service build --target wc --name patient-match ./src/components/PatientMatch.vue
```

The default WebComponent build does not include Vue. 
This command will including Vue since forView does not include Vue this is the default build command.
```
npx vue-cli-service build --inline-vue --target wc --name patient-match ./src/components/PatientMatch.vue
```



# How to reference the WebComponent
The patient-match element can be referenced like any other HTML element. 
Sample to be added

## Listening to events
Event information to be added

## NPM commands

### Compiles and hot-reloads test cases for development
```
npm run serve
```

### Compiles and minifies WebComponent for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```
